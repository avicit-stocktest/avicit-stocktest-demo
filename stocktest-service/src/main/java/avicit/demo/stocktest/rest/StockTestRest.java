package avicit.demo.stocktest.rest;

import avicit.business.core.contextThread.ContextCommonHolder;
import avicit.business.core.excel.export.ServerExcelExport;
import avicit.business.core.excel.export.headersource.VueExportDataGridHeaderSource;
import avicit.business.core.rest.msg.QueryReqBean;
import avicit.business.core.rest.msg.QueryRespBean;
import avicit.business.core.rest.msg.ResponseMsg;
import avicit.business.core.utils.JsonHelper;
import avicit.business.core.utils.StringUtils;
import avicit.business.module.system.SystemConstant;
import avicit.business.module.system.client.ConvertColumnClient;
import avicit.demo.stocktest.dto.StockTestDTO;
import avicit.demo.stocktest.service.StockTestService;
import com.fasterxml.jackson.core.type.TypeReference;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static avicit.business.core.utils.BusinessUtil.buildExcelFile;
import static avicit.business.core.utils.BusinessUtil.getSortExpColumnName;
import static avicit.business.module.system.client.ConvertColumnClient.convertFormat;
import static avicit.business.module.system.client.ConvertColumnClient.createConvertSet;

/**
* @金航数码科技有限责任公司
* @作者：guomx
* @邮箱：mm768528@163.com
* @创建时间： 2020-11-26 09:30
* @类说明：stock_testRest
* @修改记录：
*/
@RestController
@Api(tags = "stockTest", description = "stock_test")
@RequestMapping("/api/demo/stocktest/StockTestRest")
public class StockTestRest {

	private static final Logger LOGGER = LoggerFactory.getLogger(StockTestRest.class);

	@Autowired
	private StockTestService stockTestService;

	@Autowired
	private ConvertColumnClient convertColumnClient;

	/**
	* 按条件分页查询
	* @param queryReqBean 查询条件
	* @return ResponseMsg<QueryRespBean < StockTestDTO>>
	*/
	@PostMapping("/searchByPage/v1")
	@ApiOperation(value = "按条件分页查询")
	public ResponseMsg<QueryRespBean<StockTestDTO>> searchByPage(@ApiParam(value = "查询条件", name = "queryReqBean") @RequestBody QueryReqBean<StockTestDTO> queryReqBean) {
		ResponseMsg<QueryRespBean<StockTestDTO>> responseMsg = new ResponseMsg<>();
		if (StringUtils.isNotEmpty(queryReqBean.getSidx()) && StringUtils.isNotEmpty(queryReqBean.getSord())) {
			String sordExp = getSortExpColumnName(queryReqBean.getSidx(), queryReqBean.getSord(), StockTestDTO.class);
			if (StringUtils.isNotEmpty(sordExp)) {
				queryReqBean.setSortExp(sordExp);
			}
		}
		if (StringUtils.isNotEmpty(queryReqBean.getKeyWord())) {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			StockTestDTO searchKeyWordParam = JsonHelper.getInstance().readValue(queryReqBean.getKeyWord(), dateFormat, new TypeReference<StockTestDTO>() {
			});
			queryReqBean.setSearchParams(searchKeyWordParam);
		}
		//获取token
		String token = ContextCommonHolder.getToken();
		//文档密级
		String wordSecret = ContextCommonHolder.getWordSecret();
		QueryRespBean<StockTestDTO> result = stockTestService.searchStockTestByPage(queryReqBean, ContextCommonHolder.getOrgId(), wordSecret, queryReqBean.getSortExp());
		responseMsg.setResponseBody(result);
		return responseMsg;
	}

	/**
	* 按条件不分页查询
	*
	* @param queryReqBean 查询条件
	* @return ResponseMsg<List < StockTestDTO>>
	*/
	@PostMapping("/search/v1")
	@ApiOperation(value = "按条件不分页查询")
	public ResponseMsg<List<StockTestDTO>> search(@ApiParam(value = "查询条件", name = "queryReqBean") @RequestBody QueryReqBean<StockTestDTO> queryReqBean) {
		ResponseMsg<List<StockTestDTO>> responseMsg = new ResponseMsg<>();
		List<StockTestDTO> result = stockTestService.searchStockTest(queryReqBean);
		responseMsg.setResponseBody(result);
		return responseMsg;
	}

	/**
	* 按条件导出
	*
	* @param serverExp 查询条件
	* @return ResponseMsg<List < StockTestDTO>>
	*/
	@PostMapping("/exportServerData/v1")
	@ApiOperation(value = "按条件导出")
	public void exportServerData(@ApiParam(value = "导出条件", name = "serverExcelExport") @RequestBody ServerExcelExport<StockTestDTO> serverExp, HttpServletResponse response) throws Exception {
		QueryReqBean<StockTestDTO> queryReqBean = serverExp.getQueryReqBean();
		//文档密级
		String wordSecret = ContextCommonHolder.getWordSecret();
		List<StockTestDTO> result = stockTestService.searchStockTestForExportExcel(queryReqBean, ContextCommonHolder.getOrgId(), wordSecret, queryReqBean.getSortExp());
		VueExportDataGridHeaderSource exportDataGridHeaderSource = new VueExportDataGridHeaderSource(serverExp.getDataGridheaders());
		exportDataGridHeaderSource.setUnexportColumn(serverExp.getUnContainFields());
		serverExp.setUserDefinedGridHeader(exportDataGridHeaderSource);
		Workbook workbook = serverExp.exportData(result);
		//生成excel文件并进行下载
		buildExcelFile(workbook, serverExp.getSheetName(), response);
	}

	/**
	* 通过主键查询单条记录
	*
	* @param id 主键id
	* @return ResponseMsg<StockTestDTO>
	*/
	@GetMapping("/get/v1/{id}")
	@ApiOperation(value = "通过主键查询单条记录")
	public ResponseMsg<StockTestDTO> get(@ApiParam(value = "主键id", name = "id") @PathVariable("id") String id) {
		ResponseMsg<StockTestDTO> responseMsg = new ResponseMsg<>();
		StockTestDTO stockTest = stockTestService.queryStockTestByPrimaryKey(id);
		responseMsg.setResponseBody(stockTest);
		return responseMsg;
	}

	/**
	* 新增对象
	*
	* @param stockTest 保存对象
	* @return ResponseMsg<String>
	*/
	@PostMapping("/save/v1")
	@ApiOperation(value = "新增对象")
	public ResponseMsg<String> save(@ApiParam(value = "保存对象", name = "stockTest") @RequestBody StockTestDTO stockTest) {
		ResponseMsg<String> responseMsg = new ResponseMsg<>();
		stockTest.setOrgIdentity(ContextCommonHolder.getOrgId());
		if (StringUtils.isEmpty(stockTest.getId())) {
			responseMsg.setResponseBody(stockTestService.insertStockTest(stockTest));
		} else {
			responseMsg.setResponseBody(String.valueOf(stockTestService.updateStockTest(stockTest)));
		}
		return responseMsg;
	}

	/**
	* 修改部分对象字段
	*
	* @param stockTest 修改对象
	* @return ResponseMsg<Integer>
	*/
	@PostMapping("/updateSensitive/v1")
	@ApiOperation(value = "修改部分对象字段")
	public ResponseMsg<Integer> updateSensitive(@ApiParam(value = "修改对象", name = "stockTest") @RequestBody StockTestDTO stockTest) {
		ResponseMsg<Integer> responseMsg = new ResponseMsg<>();
		responseMsg.setResponseBody(stockTestService.updateStockTestSensitive(stockTest));
		return responseMsg;
	}

	/**
	* 修改全部对象字段
	*
	* @param stockTest 修改对象
	* @return ResponseMsg<Integer>
	*/
	@PostMapping("/updateAll/v1")
	@ApiOperation(value = "修改全部对象字段")
	public ResponseMsg<Integer> updateAll(@ApiParam(value = "修改对象", name = "stockTest") @RequestBody StockTestDTO stockTest) {
		ResponseMsg<Integer> responseMsg = new ResponseMsg<>();
		responseMsg.setResponseBody(stockTestService.updateStockTest(stockTest));
		return responseMsg;
	}

	/**
	* 按主键单条删除
	*
	* @param id 主键id
	* @return ResponseMsg<Integer>
	*/
	@PostMapping("/deleteById/v1")
	@ApiOperation(value = "按主键单条删除")
	public ResponseMsg<Integer> deleteById(@ApiParam(value = "主键id", name = "id") @RequestBody String id) {
		ResponseMsg<Integer> responseMsg = new ResponseMsg<>();
		responseMsg.setResponseBody(stockTestService.deleteStockTestById(id));
		return responseMsg;
	}

	/**
	* 批量删除
	*
	* @param ids 逗号分隔的id串
	* @return ResponseMsg<Integer>
	*/
	@PostMapping("/deleteByIds/v1")
	@ApiOperation(value = "批量删除")
	public ResponseMsg<Integer> deleteByIds(@ApiParam(value = "逗号分隔的id串", name = "ids") @RequestBody String ids) {
		ResponseMsg<Integer> responseMsg = new ResponseMsg<>();
		responseMsg.setResponseBody(stockTestService.deleteStockTestByIds(ids.split(",")));
		return responseMsg;
	}

}
