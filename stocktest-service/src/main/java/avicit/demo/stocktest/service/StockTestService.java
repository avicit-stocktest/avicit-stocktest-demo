package avicit.demo.stocktest.service;

import avicit.business.core.exception.BusinessException;
import avicit.business.core.log.SysLogUtil;
import avicit.business.core.mybatis.pagehelper.PageHelper;
import avicit.business.core.properties.PlatformConstant;
import avicit.business.core.rest.msg.QueryReqBean;
import avicit.business.core.rest.msg.QueryRespBean;
import avicit.business.core.utils.ComUtil;
import avicit.business.core.utils.PojoUtil;
import avicit.business.core.utils.StringUtils;
import avicit.demo.stocktest.dao.StockTestDAO;
import avicit.demo.stocktest.dto.StockTestDTO;
import com.github.pagehelper.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
* @金航数码科技有限责任公司
* @作者：guomx
* @邮箱：mm768528@163.com
* @创建时间： 2020-11-26 09:30
* @类说明：stock_testService
* @修改记录：
*/
@Service
public class StockTestService {

	private static final Logger LOGGER = LoggerFactory.getLogger(StockTestService.class);

	@Autowired
	private StockTestDAO stockTestDAO;


	/**
	* 按条件分页查询
	* @param queryReqBean 查询条件
	* @param orgIdentity  组织id
	* @param wordSecret   文档密级
	* @param orderBy      排序
	* @return QueryRespBean<StockTestDTO>
	*/
	@Transactional(readOnly = true)
	public QueryRespBean<StockTestDTO> searchStockTestByPage(QueryReqBean<StockTestDTO> queryReqBean, String orgIdentity, String wordSecret, String orderBy) {
		QueryRespBean<StockTestDTO> queryRespBean = new QueryRespBean<>();
		try {
			PageHelper.startPage(queryReqBean.getPageParameter());
			StockTestDTO searchParams = queryReqBean.getSearchParams();
			Page<StockTestDTO> dataList = stockTestDAO.searchStockTestByPage(searchParams, orgIdentity, wordSecret, orderBy, queryReqBean.getKeyWord());
			queryRespBean.setResult(dataList);
			return queryRespBean;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("查询失败");
		}
	}

	/**
	* 按条件查询
	*
	* @param queryReqBean 查询条件
	* @return List<StockTestDTO>
	*/
	@Transactional(readOnly = true)
	public List<StockTestDTO> searchStockTest(QueryReqBean<StockTestDTO> queryReqBean) {
		try {
			return stockTestDAO.searchStockTest(queryReqBean.getSearchParams());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("查询失败");
		}
	}

	/**
	* 按条件导出
	*
	* @param queryReqBean 查询条件
	* @param orgIdentity  组织id
	* @param wordSecret   文档密级
	* @param orderBy      排序
	* @return List<StockTestDTO>
	*/
	@Transactional(readOnly = true)
	public List<StockTestDTO> searchStockTestForExportExcel(QueryReqBean<StockTestDTO> queryReqBean, String orgIdentity, String wordSecret, String orderBy) {
		try {
			return stockTestDAO.searchStockTestForExportExcel(queryReqBean.getSearchParams(), orgIdentity, wordSecret, orderBy, queryReqBean.getKeyWord());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("导出失败");
		}
	}

	/**
	* 通过主键查询单条记录
	*
	* @param id 主键id
	* @return StockTestDTO
	*/
	@Transactional(readOnly = true)
	public StockTestDTO queryStockTestByPrimaryKey(String id) {
		try {
			StockTestDTO stockTestDTO = stockTestDAO.findStockTestById(id);
			//记录日志
			if (stockTestDTO != null) {
				SysLogUtil.log4Query(stockTestDTO);
			}
			return stockTestDTO;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("通过主键查询单条记录失败");
		}
	}

	/**
	* 新增对象
	*
	* @param stockTestDTO 保存对象
	* @return String
	*/
	@Transactional
	public String insertStockTest(StockTestDTO stockTestDTO) {
		try {
			stockTestDTO.setId(ComUtil.getId());
			PojoUtil.setSysProperties(stockTestDTO, PlatformConstant.OpType.insert);
			stockTestDAO.insertStockTest(stockTestDTO);
			//记录日志
			SysLogUtil.log4Insert(stockTestDTO);
			return stockTestDTO.getId();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("新增对象失败");
		}
	}

	/**
	* 批量新增对象
	*
	* @param dtoList 保存对象集合
	* @return int
	*/
	@Transactional
	public int insertStockTestList(List<StockTestDTO> dtoList) {
		List<StockTestDTO> beanList = new ArrayList<>();
		for (StockTestDTO stockTestDTO : dtoList) {
			stockTestDTO.setId(ComUtil.getId());
			PojoUtil.setSysProperties(stockTestDTO, PlatformConstant.OpType.insert);
			//记录日志
			SysLogUtil.log4Insert(stockTestDTO);
			beanList.add(stockTestDTO);
		}
		try {
			return stockTestDAO.insertStockTestList(beanList);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("批量新增对象失败");
		}
	}

	/**
	* 修改对象全部字段
	*
	* @param stockTestDTO 修改对象
	* @return int
	*/
	@Transactional
	public int updateStockTest(StockTestDTO stockTestDTO) {
		try {
			int ret = stockTestDAO.updateStockTestAll(getUpdateDto(stockTestDTO));
			if (ret == 0) {
				throw new BusinessException("数据失效，请重新更新");
			}
			return ret;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("修改对象失败");
		}
	}

	/**
	* 修改对象部分字段
	*
	* @param stockTestDTO 修改对象
	* @return int
	*/
	@Transactional
	public int updateStockTestSensitive(StockTestDTO stockTestDTO) {
		try {
			int count = stockTestDAO.updateStockTestSensitive(getUpdateDto(stockTestDTO));
			if (count == 0) {
				throw new BusinessException("数据失效，请重新更新");
			}
			return count;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("修改对象失败");
		}
	}

	/**
	* 内部方法，获取修改的dto对象
	*
	* @param stockTestDTO
	* @return
	*/
	private StockTestDTO getUpdateDto(StockTestDTO stockTestDTO) {
		StockTestDTO oldDTO = findById(stockTestDTO.getId());
		if (oldDTO == null) {
			throw new BusinessException("数据不存在");
		}
		//记录日志
		SysLogUtil.log4Update(stockTestDTO, oldDTO);
		PojoUtil.setSysProperties(stockTestDTO, PlatformConstant.OpType.update);
		PojoUtil.copyProperties(oldDTO, stockTestDTO, true);
		return oldDTO;
	}

	/**
	* 批量更新对象
	*
	* @param dtoList 修改对象集合
	* @return int
	*/
	@Transactional
	public int updateStockTestList(List<StockTestDTO> dtoList) {
		List<StockTestDTO> beanList = new ArrayList<>();
		for (StockTestDTO stockTestDTO : dtoList) {
			StockTestDTO oldDTO = getUpdateDto(stockTestDTO);
			beanList.add(oldDTO);
		}
		try {
			return stockTestDAO.updateStockTestList(beanList);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("批量更新对象失败");
		}
	}

	/**
	* 按主键单条删除
	*
	* @param id 主键id
	* @return int
	*/
	@Transactional
	public int deleteStockTestById(String id) {
		if (StringUtils.isEmpty(id)) {
			throw new BusinessException("删除失败！传入的参数主键为null");
		}
		try {
			//记录日志
			StockTestDTO stockTestDTO = findById(id);
			if (stockTestDTO == null) {
				throw new BusinessException("删除失败！对象不存在");
			}
			SysLogUtil.log4Delete(stockTestDTO);
			//删除业务数据
			int count = stockTestDAO.deleteStockTestById(id);
			return count;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("删除失败");
		}
	}

	/**
	* 批量删除数据
	*
	* @param ids id的数组
	* @return int
	*/
	@Transactional
	public int deleteStockTestByIds(String[] ids) {
		int result = 0;
		for (String id : ids) {
			deleteStockTestById(id);
			result++;
		}
		return result;
	}

	/**
	* 日志专用，内部方法，不再记录日志
	*
	* @param id 主键id
	* @return StockTestDTO
	*/
	private StockTestDTO findById(String id) {
		return stockTestDAO.findStockTestById(id);
	}
}
