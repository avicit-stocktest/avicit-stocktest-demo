package avicit.demo.stocktest.dao;

import avicit.demo.stocktest.dto.StockTestDTO;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @金航数码科技有限责任公司
* @作者：guomx
* @邮箱：mm768528@163.com
* @创建时间： 2020-11-26 09:30
* @类说明：stock_testDao
* @修改记录：
*/
public interface StockTestDAO {

	/**
	* 分页查询
	* @param stockTestDTO 查询对象
	* @param orgIdentity       组织id
	* @param wordSecret        文档密级
	* @param orderBy           排序条件
	* @param keyWords          查询关键字
	* @return Page<StockTestDTO>
	*/
	public Page<StockTestDTO> searchStockTestByPage(@Param("bean") StockTestDTO stockTestDTO, @Param("orgIdentity") String orgIdentity, @Param("wordSecret") String wordSecret, @Param("orderBy") String orderBy, @Param("keyWords") String keyWords);

	/**
	* 不分页查询
	* @param stockTestDTO 查询对象
	* @return List<StockTestDTO>
	*/
	public List<StockTestDTO> searchStockTest(@Param("bean") StockTestDTO stockTestDTO);

	/**
	* 按条件导出查询
	* @param stockTestDTO 查询对象
	* @param orgIdentity       组织id
	* @param wordSecret        文档密级
	* @param orderBy           排序条件
	* @param keyWords          查询关键字
	* @return Page<StockTestDTO>
	*/
	public List<StockTestDTO> searchStockTestForExportExcel(@Param("bean") StockTestDTO stockTestDTO, @Param("orgIdentity") String orgIdentity, @Param("wordSecret") String wordSecret, @Param("orderBy") String orderBy, @Param("keyWords") String keyWords);

	/**
	* 主键查询
	* @param id 主键id
	* @return StockTestDTO
	*/
	public StockTestDTO findStockTestById(String id);

	/**
	* 新增
	* @param stockTestDTO 保存对象
	* @return int
	*/
	public int insertStockTest(StockTestDTO stockTestDTO);

	/**
	* 批量新增
	* @param dtoList 保存对象集合
	* @return int
	*/
	public int insertStockTestList(@Param("dtoList") List<StockTestDTO> dtoList);

	/**
	* 更新部分对象
	* @param stockTestDTO 更新对象
	* @return int
	*/
	public int updateStockTestSensitive(StockTestDTO stockTestDTO);

	/**
	* 更新全部对象
	* @param stockTestDTO 更新对象
	* @return int
	*/
	public int updateStockTestAll(StockTestDTO stockTestDTO);

	/**
	* 批量更新对象
	* @param dtoList 批量更新对象集合
	* @return int
	*/
	public int updateStockTestList(@Param("dtoList") List<StockTestDTO> dtoList);

	/**
	* 按主键删除
	* @param id 主键id
	* @return int
	*/
	public int deleteStockTestById(String id);
}
