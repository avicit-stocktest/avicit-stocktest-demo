FROM openjdk:8-alpine
MAINTAINER avicit
COPY /stocktest-service/target/stocktest-service.jar app.jar
CMD java -jar app.jar

