package avicit.demo.stocktest.api;

import avicit.business.core.rest.client.RestClient;
import avicit.business.core.rest.client.RestClientUtils;
import avicit.business.core.rest.msg.QueryReqBean;
import avicit.business.core.rest.msg.QueryRespBean;
import avicit.business.core.rest.msg.ResponseMsg;
import avicit.demo.stocktest.dto.StockTestDTO;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
* @金航数码科技有限责任公司
* @作者：guomx
* @邮箱：mm768528@163.com
* @创建时间： 2020-11-26 09:30
* @类说明：stock_testApi
* @修改记录：
*/
@Component
public class StockTestApi {

	/**
	* 服务编码
	*/
	private static final String SERVICE_CODE = "stocktest";
	private static final String BASE_PATH = "/api/demo/stocktest/stockTestRest";

	@Autowired
	private RestClient restClient;

	/**
	* 按条件分页查询
	*
	* @param queryReqBean 查询条件
	* @return QueryRespBean<StockTestDTO>
	*/
	public QueryRespBean<StockTestDTO> searchByPage(QueryReqBean<StockTestDTO> queryReqBean) {
		String url = BASE_PATH + "/searchByPage/v1";
		ResponseMsg<QueryRespBean<StockTestDTO>> responseMsg = restClient.doPost(SERVICE_CODE, url, queryReqBean, new ParameterizedTypeReference<ResponseMsg<QueryRespBean<StockTestDTO>>>() {
		});
		return RestClientUtils.getResponseBody(responseMsg);
	}

	/**
	* 按条件不分页查询
	*
	* @param queryReqBean 查询条件
	* @return List <StockTestDTO>
	*/
	public List<StockTestDTO> search(QueryReqBean<StockTestDTO> queryReqBean) {
		String url = BASE_PATH + "/search/v1";
		ResponseMsg<List<StockTestDTO>> responseMsg = restClient.doPost(SERVICE_CODE, url, queryReqBean, new ParameterizedTypeReference<ResponseMsg<List<StockTestDTO>>>() {
		});
		return RestClientUtils.getResponseBody(responseMsg);
	}

	/**
	* 通过主键查询单条记录
	*
	* @param id 主键id
	* @return StockTestDTO
	*/
	public StockTestDTO get(String id) {
		String url = BASE_PATH + "/get/v1/" + id;
		ResponseMsg<StockTestDTO> responseMsg = restClient.doGet(SERVICE_CODE, url, new ParameterizedTypeReference<ResponseMsg<StockTestDTO>>() {
		});
		return RestClientUtils.getResponseBody(responseMsg);
	}

	/**
	* 新增对象
	*
	* @param stockTestDTO 保存对象
	* @return String
	*/
	public String save(StockTestDTO stockTest) {
		String url = BASE_PATH + "/save/v1";
		ResponseMsg<String> responseMsg = restClient.doPost(SERVICE_CODE, url, stockTest, new ParameterizedTypeReference<ResponseMsg<String>>() {
		});
		return RestClientUtils.getResponseBody(responseMsg);
	}

	/**
	* 修改部分对象字段
	*
	* @param stockTestDTO 修改对象
	* @return Integer
	*/
	public Integer updateSensitive(StockTestDTO stockTest) {
		String url = BASE_PATH + "/updateSensitive/v1";
		ResponseMsg<Integer> responseMsg = restClient.doPost(SERVICE_CODE, url, stockTest, new ParameterizedTypeReference<ResponseMsg<Integer>>() {
		});
		return RestClientUtils.getResponseBody(responseMsg);
	}

	/**
	* 修改全部对象字段
	*
	* @param stockTestDTO 修改对象
	* @return Integer
	*/
	public Integer updateAll(StockTestDTO stockTest) {
		String url = BASE_PATH + "/updateAll/v1";
		ResponseMsg<Integer> responseMsg = restClient.doPost(SERVICE_CODE, url, stockTest, new ParameterizedTypeReference<ResponseMsg<Integer>>() {
		});
		return RestClientUtils.getResponseBody(responseMsg);
	}

	/**
	* 按主键单条删除
	*
	* @param id 主键id
	* @return Integer
	*/
	public Integer deleteById(String id) {
		String url = BASE_PATH + "/deleteById/v1";
		ResponseMsg<Integer> responseMsg = restClient.doPost(SERVICE_CODE, url, id, new ParameterizedTypeReference<ResponseMsg<Integer>>() {
		});
		return RestClientUtils.getResponseBody(responseMsg);
	}

	/**
	* 批量删除
	*
	* @param ids 逗号分隔的id串
	* @return Integer
	*/
	public Integer deleteByIds(String ids) {
		String url = BASE_PATH + "/deleteByIds/v1";
		ResponseMsg<Integer> responseMsg = restClient.doPost(SERVICE_CODE, url, ids, new ParameterizedTypeReference<ResponseMsg<Integer>>() {
		});
		return RestClientUtils.getResponseBody(responseMsg);
	}
}
